# mumablue

# Instrucciones:

- Clonar el proyecto mumablue
- Con Docker Instalado y situados en la raiz del proyecto ejecutamos el siguiente comando: docker build . -t mumablue:latest
- Por último cuando acabe de construir la imagen ejecutamos el siguiente comando : docker run -d -p 80:80 mumablue:latest

# Descripción

He realizado la app en Angular 8, he usado vuestra api y he consumido el servicio para obtener productos 
(he dejado las funciones listas para poder usar el paginado), y obtener paises, para los lenguajes he consumido una api propia en la
que he montado una tabla languages en una bd MariaDb en la que he añadido los campos (id, code, locale, name) y he añadido
los lenguajes sacados de vuestra api, he creado un servicio paginado que devuelve los lenguajes usando Symfony 4 que lo tengo alojado en el 
hosting para mayor comodidad en su consumo.

En la parte front he añadido animaciones y he usado Bootstrap para el diseño.

He intentado usar distintos elementos para mostrar los datos como una pantalla modal y en el filtro he usado tanto input, 
checkbox y select para que el formulario fuera variado.

He disfrutado realizando el test y espero que os guste.
Un saludo

