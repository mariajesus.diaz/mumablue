import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ProductModel } from './models/product.model';
import { ApiService } from '../app/service/api.service';
import { map, catchError } from 'rxjs/operators';
import { FiltroModel } from './models/filtro.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public products$: Observable<ProductModel[]>;
  public title = 'front';
  public filtro: FiltroModel;

  /**
	  * Component constructor
	  * @param apiService: ApiService
	  */
  constructor(private apiService: ApiService
  ) {

  }

  ngOnInit() {
    this.getProducts();
  }

  filter($event) {
    this.filtro = $event;
    this.products$ = this.apiService.getProducts().pipe(
      map(response =>  response['hydra:member']),
      map(result => this.filtro.country ? result = result.filter(p => {
        for (const country of p.countries) {
          if (country.substring(country.length - 3) === this.filtro.country) {
            return true;
          }
        }
      }) : result),
      map(result => this.filtro.availableAges.length > 0 ? result = result.filter(p => {
        if (p.availableAges) {
          for (const availableAge of p.availableAges) {
            for (const favailableAge of this.filtro.availableAges) {
              if (favailableAge === availableAge) {
                return true;
              }
            }
          }
        }
      }) : result),
      map(result => this.filtro.language ?
        result = result.filter(p => p.variations.length > 0 ?  p.variations[0].language.code === this.filtro.language : false) :
        result),
      map(result => this.filtro.people ? result = result.filter(p => p.variations.length > 0 ?
        p.variations[0].people === this.filtro.people : result)
        : result),
      map(result => this.filtro.type ? result = result.filter(p => p.type === this.filtro.type) : result));
  }

  getProducts(pagination = 'false', page = '', itemPerPage = '') {
    this.products$ = this.apiService.getProducts(pagination, page, itemPerPage ).pipe(
      map(response =>  response['hydra:member']),
      catchError(this.handleError('Not Found', [])));
  }


  /*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
   private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // Let the app keep running by returning an empty result.
        return of(result);
    };
  }

}
