import { Component, OnInit, Input, HostListener, ViewChild } from '@angular/core';
import { ProductModel } from '../models/product.model';
import { Observable, of } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';
import { trigger, style, transition, animate, state } from '@angular/animations';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css'],
  animations : [
    trigger('enterState', [
      state('void', style({
        transform: 'translateX(-100%)',
        opacity: 0
      })),
      transition(':enter', [
        animate(700, style({
          transform: 'translateX(0)',
          opacity: 1
        }))
      ])
    ])
  ]
})
export class CatalogComponent  implements OnInit  {

  // Variables públicas
  @Input() filter: any[];
  @Input() p: ProductModel[];
  public showNavigationArrows = false;
  public showNavigationIndicators = false;
  public showGoUpButton: boolean;
  public showScrollHeight = 400;
  public hideScrollHeight = 200;
  public escala: string;

  @ViewChild(CdkVirtualScrollViewport, {static : true})
  viewport: CdkVirtualScrollViewport;

  /**
	  * Component constructor
	  *
	  */
  constructor() {
    this.showGoUpButton = false;
  }

  escalar(event) {
    this.escala = 'escalar';
  }

  desescalar() {
    this.escala = 'desescalar';
  }

  go() {
    this.viewport.scrollToIndex(23);
  }

  ngOnInit() {
  }

  scrollTop() {
    document.body.scrollTop = 0; // Safari
    document.documentElement.scrollTop = 0; // Otros
  }

  /*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
   private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // Let the app keep running by returning an empty result.
        return of(result);
    };

  }
@HostListener('window:scroll', [])
  onWindowScroll() {
    if (( window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop) > this.showScrollHeight) {
      this.showGoUpButton = true;
    } else if ( this.showGoUpButton &&
      (window.pageYOffset ||
        document.documentElement.scrollTop ||
        document.body.scrollTop)
      < this.hideScrollHeight) {
      this.showGoUpButton = false;
    }
  }
}
