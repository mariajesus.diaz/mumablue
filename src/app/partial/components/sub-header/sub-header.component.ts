import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ApiService } from 'src/app/service/api.service';
import { LanguageModel } from 'src/app/models/language.model';
import { CountryModel } from 'src/app/models/country.model';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent implements OnInit {

  @Output() seleccion = new EventEmitter();
  // Variables públicas
  public languages: LanguageModel[];
  public countries: CountryModel[];
  public filterForm: FormGroup;
  public selectedAges: any [];

  /**
   * Component constructor
   * @param apiService: ApiService
   * @param fb: FormBuilder
   *
   */
  constructor(
    private apiService: ApiService,
    private fb: FormBuilder
    ) {
      this.selectedAges = [];
    }

  ngOnInit() {
    this.initLoginForm();
    this.apiService.getLanguages().subscribe(
      response => {
        this.languages =  response.language.items;
      },
      error => {
        console.log('error');
      }
    );

    this.apiService.getCountries().subscribe(
      response => {
        this.countries =  response['hydra:member'];
      },
      error => {
        console.log('error');
      }
    );
  }

  initLoginForm() {
      this.filterForm = this.fb.group({
        type: [''],
        language: [''],
        country: [''],
        availableAges: this.fb.array([]),
        people: [''],
      });
  }

  submit() {
    const controls = this.filterForm.controls;
    const campos = {
      country: controls.country.value,
      language: controls.language.value,
      people: controls.people.value,
      type: controls.type.value,
      availableAges: this.selectedAges
    };
    this.seleccion.emit(campos);
  }

  onChange(age: string, isChecked: boolean) {
    if (isChecked) {
      this.selectedAges.push(age);
    } else {
      const index =  this.selectedAges.findIndex(x => x.value === age);
      this.selectedAges.splice(index, 1);
    }
  }

   /*
 	 * Handle Http operation that failed.
 	 * Let the app continue.
     *
	 * @param operation - name of the operation that failed
 	 * @param result - optional value to return as the observable result
 	 */
   private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
        // TODO: send the error to remote logging infrastructure
        console.error(error); // log to console instead

        // Let the app keep running by returning an empty result.
        return of(result);
    };

  }

}
