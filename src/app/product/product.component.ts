import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { ProductModel } from '../models/product.model';
import { trigger, style, transition, animate, state } from '@angular/animations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations : [
    trigger('Esc', [
      state(
        'escalar',
        style({
          transform: 'scale(1.2)',
        })
      ),
      state(
        'desescalar',
        style({
          transform: 'scale(1)',
        })
      ),
      transition('* => *', animate('1000ms ease')),
    ]),
  ]
})
export class ProductsComponent implements OnInit {

  @Input() product: ProductModel;
  public escala: string;

  constructor(private modalService: NgbModal) { }

  escalar(event) {
    this.escala = 'escalar';
  }

  desescalar() {
    this.escala = 'desescalar';
  }

  openWindowCustomClass(content) {
    this.modalService.open(content, { centered: true, windowClass: 'dark-modal' });
  }

  ngOnInit() {
  }

}
