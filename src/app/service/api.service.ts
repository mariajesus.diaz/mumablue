import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { ProductModel } from '../models/product.model';
import { LanguageModel } from '../models/language.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getProducts(pagination= 'false', page = '', itemperpage = ''): Observable<any> {
    const httpHeaders = new HttpHeaders()
                      .set('Content-Type', 'application/json; charset=UTF-8');
    const httpParams = new HttpParams()
                  .set('page', page)
                  .set ('itemsPerPage', itemperpage)
                  .set('pagination', pagination);
    return this.http.get<ProductModel[]>( environment.apiUrl + 'products',
    { headers: httpHeaders,
    observe: 'response',
    params: httpParams
    }
    ).pipe(
      map( response => {
        if (response.status === 200) {
              return response.body;
        } else {
            return null;
        }
    }));

  }

  getLanguages(): Observable<any> {
    const httpHeaders = new HttpHeaders()
                      .set('Content-Type', 'application/json; charset=UTF-8');
    return this.http.get<LanguageModel[]>( environment.apiPropiaUrl + 'language/list',
    { headers: httpHeaders,
    observe: 'response'
    }
    ).pipe(
      map( response => {
        if (response.status === 200) {
              return response.body;
        } else {
            return null;
        }
    }));

  }
getCountries(): Observable<any> {
    const httpHeaders = new HttpHeaders()
                      .set('Content-Type', 'application/json; charset=UTF-8');
    return this.http.get<LanguageModel[]>( environment.apiUrl + 'countries',
    { headers: httpHeaders,
    observe: 'response'
    }
    ).pipe(
      map( response => {
        if (response.status === 200) {
              return response.body;
        } else {
            return null;
        }
    }));

  }

}
