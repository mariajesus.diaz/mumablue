import { VariationModel } from './variation.model';

export class ProductModel {
  id: number;
  type: string;
  variations: VariationModel[];
  bookTotalPages: number;
  availableAges: string[];
  namePanel: string;
  priceCurrency: string;
  countries: [];

  constructor() {
  }

}
