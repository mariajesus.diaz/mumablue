import { LanguageModel } from './language.model';
import { VariationImageModel } from './variationImage.model';

export class VariationModel {
  id: number;
  description: string;
  language: LanguageModel;
  name: string;
  people: string;
  synopsis: string;
  variationImage: VariationImageModel;

  constructor() {
  }

}
