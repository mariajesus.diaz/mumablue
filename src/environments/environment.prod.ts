export const environment = {
  production: true,
  apiUrl: 'https://staging-k8s-api.mumablue.com/api/',
  apiPropiaUrl : 'http://dev.appogeodigital.com/api/'
};
