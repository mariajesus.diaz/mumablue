# Stage 0 Node.js
FROM node:latest as node
WORKDIR /app
COPY ./ /app/
RUN npm install
RUN npm run build --production

# Stage 1 Nginx
FROM nginx:alpine
COPY --from=node /app/dist/mumablue /usr/share/nginx/html
